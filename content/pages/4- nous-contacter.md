title: Nous contacter
Category: Nous contacter
Tags: municipales
Slug: nous-contacter

|                                                 |                                                                        |
| ----------------------------------------------- | ---------------------------------------------------------------------- |
| <i class="fab fa-facebook-square"></i> Facebook | [@CauseCommuneClermont](https://www.facebook.com/CauseCommuneClermont) |
| <i class="fab fa-twitter-square"></i> Twitter   | [@cause_commune](https://www.twitter.com/cause_commune)                |
| <i class="fab fa-snapchat-square"></i> Snapchat | [@cause_commune](https://www.snapchat.com/add/cause_commune)           |
| <i class="fab fa-instagram"></i> Instagram      | [cause_commune](https://www.instagram.com/cause_commune/)              |
| <i class="fab fa-youtube-square"></i> Youtube   | [www.youtube.com/channel/UCTuszJyLpB_4L-MvyTUC0fA](https://www.youtube.com/channel/UCTuszJyLpB_4L-MvyTUC0fA) |
| <i class="fas fa-envelop"></i> Mail             | [faisons.causecommune@gmail.com](mailto:faisons.causecommune@gmail.com)|
| <i class="fas fa-home"></i> Adresse postale     | 21, rue du Port 63 000 Clermont-Ferrand                                |
| <i class="fas fa-phone"></i> Téléphone          | (à venir !)                                                            |