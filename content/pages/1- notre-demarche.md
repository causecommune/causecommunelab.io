Title: Notre démarche
Category: Notre Démarche
Tags: municipales
Slug: notre-demarche

<center><iframe width="560" height="315" src="https://www.youtube.com/embed/mX9nto-cWq8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

C'est la toute première fois qu'une liste éco-citoyenne se présente aux élections municipales à Clermont-Ferrand. 

Indépendante de toutes organisations politiques, elle a pour vocation de **rassembler l'ensemble des clermontois qui souhaitent donner une réponse forte et concrète aux urgences climatique et sociale**. 

### En aucun cas, quels que soient les enjeux ou les opportunités, elle n'acceptera d'alliance ou de fusion au premier comme au second tour.

**Les Clermontoises et Clermontois engagés au sein de Cause Commune portent une ambition forte**  pour cette élection et [un véritable projet pour leur ville, au service de ses habitants](/category/nos-propositions.html).

Mais pour eux l'enjeu est bien supérieur à l'obtention de sièges de conseillers municipaux.

<div class="panel panel-success">
  <div class="panel-body">
    Parce que nous pensons que <strong>l'écologie et la solidarité doivent constituer le fil rouge de notre mandature pour lutter contre le réchauffement climatique</strong> au niveau local tout en garantissant la qualité de vie des plus modestes d’entre nous.
  </div>
</div>
    
<div class="panel panel-success">
  <div class="panel-body">
  Parce que nous voulons <strong>initier une nouvelle approche de la citoyenneté qui s'appuie sur la démocratie directe et horizontale</strong>. L'élaboration de notre programme, co-construit et validé par les habitants, pose déjà les bases de cette nouvelle forme de gouvernance.
  </div>
</div>

<div class="panel panel-success">
  <div class="panel-body">
  Parce qu'enfin nous avons fait le choix de présenter sur notre liste <strong>des bénévoles, des militants et des responsables d'association ancrés dans un travail de terrain</strong> et directement issus la société civile. 
  </div>
</div>

**L'ensemble de nos grandes mesures environnementales est en lien direct avec notre empreinte carbone**. 

A l'inverse des candidats traditionnels qui multiplient les effets d'annonce, nous avons fait le choix de communiquer sur des dossiers thématiques et **nous répondons à l'urgence climatique de manière transversale et pragmatique** sur tous les sujets.

Il nous semble important d'**affirmer le caractère profondément ouvert de notre démarche**. Nous refusons toutes les considérations politiciennes et nous appelons toutes les bonnes volontés qui se reconnaissent dans notre choix d’indépendance, d’écologie intégrale et d’horizontalité à nous rejoindre. 

Nous sommes persuadés que les Clermontois(es) peuvent agir par eux-mêmes car l'engagement traditionnel dans les partis est trop souvent synonyme de déception, de renoncement ou de division. **Nous voulons fédérer les énergies civiques** et donner à chacun le moyen d'apporter sa pierre à  l'édifice commun.

<center><img src="/images/pages/1-notre-demarche.jpg" alt="Notre démarche" title="Notre démarche" /></center>