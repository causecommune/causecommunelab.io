Title: Signature de l’appel SOS MAIRES
Date: 2020-01-10
Category: Actualités
Tags: municipales
Slug: sos-maire-resilience
Photo: actualites/11-sos-maire-resilience.png
Summary: Reconnu Maire en résilience par <a href="https://sosmaires.org">SOSMaires</a>

Soucieux de tendre vers une ville résiliente, le collectif Cause Commune souhaite assurer, à terme, l'auto-suffisance alimentaire de notre ville. 

Pour ce faire, nous proposons la création d'une régie municipale qui intégrera l'actuelle ferme urbaine et structurera d'autres projets de maraîchage, friches alimentaires, jardins partagés ou micro cultures en open-source. 

Notre engagement s'est formalisé avec la signature du label "maires en résilience" lancé par [SOS Maires](https://sosmaires.org/municipales-2020). 

Il prévoit notamment d'inscrire dans le [DICRIM](https://fr.calameo.com/read/000000209b0e8c2ea0a41) (document d'information communal sur tous les risques) le risque majeur d'une rupture de la chaîne de distribution alimentaire.

<a href="/images/actualites/11-sos-maire-resilience/Maire-resilience.jpeg">
<img style="width: 100%" src="/images/actualites/11-sos-maire-resilience/Maire-resilience.jpeg" alt="Signature de l’appel SOS MAIRES Pour une ville résiliente" title="Signature de l’appel SOS MAIRES Pour une ville résiliente">
</a>
