Title: Contre les violences faites aux femmes
Date: 2019-09-03
Category: Actualités
Slug: lutte-violence-femmes
Photo: actualites/01-lutte-violence-femmes.png
Summary: Une année de lutte

Les bénévoles de Cause Commune étaient présents le 3 septembre 2019 au rassemblement organisé à Clermont-Ferrand par les associations féministes, à l’occasion du triste cent-unième féminicide de l’année. Amplifions le mouvement pour alerter, accompagner et soutenir les femmes victimes de violences.
	
Le 25 novembre, nous participions également à la marche contre les violences faites aux femmes, pour dénoncer l’absence de mesures du gouvernement et les moyens insuffisants donnés localement pour venir en aide aux victimes et prévenir les féminicides.

<br />
<br />
<br />
## PHOTOS

<div id="diaporama">
<img src="/images/actualites/01-lutte-violence-femmes/1.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/2.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/3.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/4.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/5.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/6.jpg" alt="lutte violence femmes" title="lutte violence femmes">
<img src="/images/actualites/01-lutte-violence-femmes/7.jpg" alt="lutte violence femmes" title="lutte violence femmes">
</div>