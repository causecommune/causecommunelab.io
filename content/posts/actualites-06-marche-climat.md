Title: Marches pour le climat
Date: 2019-09-20
Category: Actualités
Slug: marche-climat
Photo: actualites/06-marche-climat.png
Summary: Urgence climatique

Face à l’urgence climatique, les jeunes ont les premiers commencé à alerter sur l’état de notre planète et nos modes de consommation, en organisant des manifestations internationales pour le climat. 

Cause Commune était des leurs !

Après les jeunes, les moins jeunes ! 

Nous étions également présents lors des marches pour le climat et la justice sociale, qui ont organisé l’agenda clermontois dès la rentrée de septembre ! 

Parce qu’il y a aujourd’hui urgence en matière écologique, sociale et démocratique, nous appelons à un monde meilleur, et à des politiques réellement investies des enjeux de notre temps. 

Puisque seuls les citoyens se préoccupent de l’avenir, mettons en commun nos énergies et nos idées pour faire Cause Commune !

<br />

## PHOTOS

<div id="diaporama">

<img src="/images/actualites/06-marche-climat/Marche climat 02.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 03.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 04.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 05.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 06.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 07.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 08.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 09.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 11.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 12.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 13.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 14.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 15.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 16.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 17.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 18.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 19.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 21.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 22.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 24.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 25.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 26.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 27.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 28.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 29.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 30.jpg" alt="" title="">
<img src="/images/actualites/06-marche-climat/Marche climat 31.jpg" alt="" title="">
</div>