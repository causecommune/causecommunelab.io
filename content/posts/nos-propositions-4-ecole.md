Title: &Eacute;cole
Date: 2015-01-01
Category: Nos Propositions
Photo: propositions/Ecole.png

- Un audit des bâtiments pour assurer la sobriété énergétique et réduire l'impact des canicules à venir
- La végétalisation des cours de récréation: remplacement de l'asphalte par des revêtements perméables et plantations d'arbres et massifs pour créer un cadre agréable et respirable
- Des cantines bio et locales avec option végétarienne tous les jours pour garantir une alimentation de qualité et équilibrée
- La création d'un espace citoyen permettant aux parents d'élèves d'être à l'initiative
- Une réflexion globale pour sécuriser les trajets du domicile à l'école: agents municipaux aux abords, promotion des pédibus, aménagements urbains spécifiques
- Des temps périscolaires axés sur l'écologie et le partage, en lien avec les services municipaux horticoles: création de jardins et serres pédagogiques, apprentissage du tri et du compost