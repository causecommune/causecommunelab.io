Title: Mobilités
Date: 2018-01-01
Category: Nos Propositions
Slug: mobilites
Photo: propositions/Mobilites.png

- Transports en commun gratuits pour tous
- Doublement du budget alloué au schéma cyclable
- Augmentation du nombre de techniciens en charge du schéma cyclable de 2 à 10
- Révision du PDU pour éviter les flux traversants
- Piétonnisation du centre-ville
- Soutien aux ateliers associatifs d'autoréparation vélo existants ainsi que les véloécoles
- Atelier de réparation vélo en régie municipale
- Création de couloirs de bus réservés
- Sécurisation des trajets vélobus vers les écoles
- Fermeture des rues aux alentours des écoles aux horaires d'entrées et de sorties
- Zone 30 entre la première et deuxième couronne