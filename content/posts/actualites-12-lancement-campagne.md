Title: Lancement de la campagne
Date: 2020-01-15
Category: Actualités
Tags: municipales
Slug: lancement-campagne
Photo: actualites/12-lancement-campagne.png
Summary: Meilleurs voeux!

Les équipes de la démarche Cause Commune ont appelé à une soirée de présentation des vœux 2020 et de lancement de la campagne municipale le 15 janvier 2020. 

Une salle comble rue Alexis Piron, en un beau moment de partage autour de nos propositions phares pour les élections des 15 et 22 mars prochain à Clermont-Ferrand.

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/12-lancement-campagne/1.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
<img src="/images/actualites/12-lancement-campagne/2.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
<img src="/images/actualites/12-lancement-campagne/3.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
<img src="/images/actualites/12-lancement-campagne/4.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
<img src="/images/actualites/12-lancement-campagne/5.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
<img src="/images/actualites/12-lancement-campagne/6.jpg" alt="Lancement de la campagne" title="Lancement de la campagne">
</div>