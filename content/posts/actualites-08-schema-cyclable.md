Title: Pour un vrai schéma cyclable
Date: 2019-11-25
Category: Actualités
Tags: municipales
Slug: schema-cyclable
Photo: actualites/08-schema-cyclable.png
Summary: Activisme à Clermont-Ferrand

En tant que collectif citoyen, nous avons décidé le 24 novembre de nous faire le relai des acteurs associatifs de notre ville et de participer à notre tour à l’extension du réseau et à la campagne de sensibilisation sur le manque de cohérence du schéma cyclable à Clermont-Ferrand. Bravo aux bénévoles pour leur engagement. En photos, la connexion entre les réseaux existants à proximité de l’avenue Léon Blum.

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/08-schema-cyclable/2.jpg" alt="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand" title="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand">
<img src="/images/actualites/08-schema-cyclable/5.jpg" alt="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand" title="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand">
<img src="/images/actualites/08-schema-cyclable/3.jpg" alt="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand" title="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand">
<img src="/images/actualites/08-schema-cyclable/4.jpg" alt="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand" title="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand">
<img src="/images/actualites/08-schema-cyclable/1.jpg" alt="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand" title="Pour un vrai schéma cyclable Activisme à Clermont-Ferrand">
</div>