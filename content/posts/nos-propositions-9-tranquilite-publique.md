Title: Tranquilité publique
Date: 2010-01-01
Category: Nos Propositions
Slug: tranquilite-publique
Photo: propositions/Tranquilite_publique.png

- Augmentation des effectifs de policiers municipaux à hauteur de 35 agents
- Ouverture de 3 bureaux de police municipale de quartiers 
- Réouverture du commissariat de Montferrand
- Formation et vigilance de l’ensemble de la police municipale en matière d’agressions et de harcèlement de rue
- Mise en place de patrouilles nocturnes en semaine et le weekend
- Organisation régulière de réunions et temps d'échanges avec les intervenants éducatifs