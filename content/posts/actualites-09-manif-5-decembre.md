Title: Réforme des retraites
Date: 2019-12-05
Category: Actualités
Tags: municipales
Slug: manif-5-decembre
Photo: actualites/09-manif-5-decembre.png
Summary: Manifestation du 5 Décembre

Réforme des retraites : Clermont-Ferrand parmi les villes les plus mobilisées de France ! Nous étions présents aux diverses manifestations à partir du 5 décembre.

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/09-manif-5-decembre/1.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/2.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/3.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/4.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/5.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/6.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/7.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
<img src="/images/actualites/09-manif-5-decembre/8.jpg" alt="manif du 5 decembre" title="manif du 5 decembre">
</div>