Title: La fête de rentrée de Cause Commune !
Date: 2019-09-07
Category: Actualités
Slug: fete-rentree
Photo: actualites/04-fete-rentree.png
Summary: 7 septembre 2019 

À l’occasion d’une fête de rentrée, les équipes de Cause Commune ont pu présenter la démarche au grand public à Montferrand, place Poly, autour d’un repas partagé, d’animations musicales, de débats et de diffusions de films engagés. 

Une superbe journée sous le soleil toute en partages !
<br />

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/04-fete-rentree/Fête rentrée.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 2.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 3.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 4.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 5.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 6.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Fête rentrée 7.jpg" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Prog1.png" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
<img src="/images/actualites/04-fete-rentree/Prog2.png" alt="La fête de rentrée de Cause Commune !" title="La fête de rentrée de Cause Commune !">
</div>