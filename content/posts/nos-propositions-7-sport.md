Title: Sport
Date: 2012-01-01
Category: Nos Propositions
Slug: sport
Photo: propositions/Sport.png

- Mise en place d'un financement clair et public à l'ensemble des structures associatives pour garantir émancipation et transparence
- Contrat annuel d'engagement entre la Mairie et les responsables d'associations sportives pour la valorisation du sport amateur
- Création d'un véritable statut de responsable d'association sportive offrant un soutien juridique et admnistratif
- Système de "Chèque sport" pris en charge par le CCAS en faveur des familles les plus défavorisées
- Programme d'actions de lutte contre toutes les formes de discrimination (sexisme, homophobie, etc.) et le dopage