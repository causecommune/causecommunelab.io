Title: #UrgencesEnGrève
Date: 2019-09-04
Category: Actualités
Slug: urgences-en-greve
Photo: actualites/02-urgences-en-greve.png
Summary: la santé en danger

Plus de 225 services d’urgences sont en grève depuis le mois d’avril, pour dénoncer les manques de soutien matériel et financier de l’État. 

Naturellement, Cause Commune était à leurs côtés au CHU Gabriel Montpied, l’occasion d’affirmer notre volonté de défendre les services publics et, en particulier, celui des hôpitaux.

<br />

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/KLOo7T5C4K0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>