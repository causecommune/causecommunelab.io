Title: Epicentre
Date: 2020-02-19
Category: Actualités
Tags: municipales
Slug: transition-ecologique
Photo: actualites/13-transition-ecologique.png
Summary: Rencontre sur la transition écologique mercredi 19 février 2020

Entretien enregistré dans le cadre d'une "Rencontre Candidat" à la mairie de Clermont pour les municipales 2020

Le mercredi 19 février 2020 à Epicentre Factory
Animation : Pierre Gérard
Montage et publication : Damien Caillard

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/763357552&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>