Title: L214
Date: 2019-12-20
Category: Actualités
Tags: municipales
Slug: charte-l214
Photo: actualites/10-L214.png
Summary: Signature de la charte

Suite à la sollicitation de L214 dans le cadre de la campagne "Une ville pour les animaux", nous avons signé la charte d'engagement sur des mesures concrètes pour réduire la souffrance animale. Parmi ces recommandations figurent l'intégration du bien-être animal dans l'attribution des marchés publics, l'introduction d'une option quotidienne végétarienne dans les cantines et l'interdiction des cirques détenant des animaux sauvages. La mairie actuelle fait pâle figure avec une note de 8/20.