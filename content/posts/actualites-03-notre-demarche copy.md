Title: Notre démarche
Date: 2019-09-06
Category: Actualités
Slug: notre-demarche
Photo: actualites/03-notre-demarche.png
Summary: Le clip de présentation 

Nos équipes ont passé une partie du mois d’août à tourner un clip de présentation de la démarche, pour retracer la volonté de Cause Commune de représenter l’ensemble des Clermontoises et Clermontois et de porter leur programme !

Le clip de #CauseCommune est en ligne sur les réseaux sociaux et sur YouTube ! Merci aux citoyen•ne•s devenu•e•s littéralement actrices et acteurs de la démarche.

<br />

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/7hbT-dCLC0Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</center>

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/03-notre-demarche/Tournage 1.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 2.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 3.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 4.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 5.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 7.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 8.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 9.jpg" alt="Notre Démarche Le clip de présentation" title="Notre Démarche Le clip de présentation">
<img src="/images/actualites/03-notre-demarche/Tournage 10.jpg" alt="Notre Démarche Le clip de présentation" title="lutte violence femmes">
</div>