Title: Gratuité des transports publics urbains
Date: 2019-09-23
Category: Actualités
Slug: gratuite-transport
Photo: actualites/07-gratuite-transport.png
Summary: signature de l’appel national

La démarche Cause Commune a co-signé l'appel national pour la gratuité des transports publics urbains !

<center>[Le lien vers l’appel](https://reseau-gratuite-transports.org)</center>
	
### IL Y A URGENCE CLIMATIQUE !

Les effets du changement climatique se font sentir de plus en plus concrètement, bouleversant nos conditions de vie. Les scientifiques du GIEC affirment dans leur dernier rapport qu’il faut limiter le réchauffement de la planète à 1,5°C. Hélas, après la COP21 les émissions de gaz à effet de serre continuent de progresser en France et dans le monde nous plaçant sur une trajectoire de 3°C à la fin du siècle alors qu’il faudrait les diviser par deux dès 2030 pour contenir le réchauffement climatique à 1,5°C. Alors, face à l’inaction du gouvernement, il faut de toute évidence amplifier les interventions citoyennes qui se développent en France et en Europe.

Les transports motorisés sont responsables de 30 % des émissions de CO2 et la voiture y entre pour moitié. L’étalement urbain, qui a repoussé les familles modestes en quête de logement accessible en périphérie des villes, et la concentration des emplois dans les agglomérations sont responsables de cet usage des voitures individuelles. Un développement important des transports en commun associé à leur gratuité est une alternative crédible, efficace, indispensable, à la hauteur des enjeux et des besoins.

### IL Y A URGENCE SANITAIRE !

Les particules fines et d’autres polluants – comme le dioxyde d’azote – émis par les véhicules, la production industrielle et les systèmes de chauffage sont responsables de plus de 42 000 décès prématurés par an en France. La quasi-totalité des agglomérations  françaises dépasse largement les seuils de dangerosité fixés par l’Organisation Mondiale de la Santé. En abandonnant l’usage de la voiture au profit des transports en commun, on améliore la qualité de la vie par une baisse de la pollution de l’air, des accidents de la route et des embouteillages. Les transports en commun sont donc bénéfiques pour toutes et tous, celles et ceux qui les prennent comme celles et ceux qui ne les prennent pas. Cela aussi justifie leur développement et leur accès gratuit.

### IL Y A URGENCE SOCIALE !

Le coût des transports pèse fortement sur le budget des ménages. Leur gratuité permet de changer la vie concrètement en donnant la possibilité à toutes et tous de se déplacer. Cette mesure sociale permet de redistribuer du revenu aux ménages, notamment à ceux des classes populaires, en ayant des conséquences favorables pour l’économie locale et les commerces de proximité.

### LA GRATUITÉ C’EST POSSIBLE

La gratuité des transports en commun est déjà instaurée dans une trentaine d’agglomérations en France (par exemple, Aubagne, Dunkerque) et des dizaines d’autres dans le monde. D’autres la prévoient ou lancent des études. En Estonie, après la capitale Tallinn, la gratuité s’étend à tout le territoire. Au Luxembourg, tous les transports en commun (bus, tram, train) seront gratuits en 2020. Toutes les expériences montrent que la gratuité, associée à une augmentation de l’offre, entraîne systématiquement une forte hausse de l’usage des transports en commun.

### LE VERSEMENT TRANSPORT : UNE SOURCE DE FINANCEMENT ESSENTIELLE

Les trajets domicile-travail ont une part très importante dans les déplacements quotidiens. Par ailleurs, la concentration croissante des activités dans les métropoles actuelles, résultat des demandes des grands groupes industriels et financiers, est source de congestion et d’étalement urbain. C’est donc à bon droit que la taxe « Versement Transport » (versée par les entreprises de plus de 10 salariés) constitue aujourd’hui une grande part du financement des transports collectifs publics. Cette source de financement doit non seulement être préservée, mais aussi accrue pour financer la gratuité et le développement des transports publics.

### LA MOBILITÉ EST UN DROIT

Pour garantir à toutes et à tous l’accès libre aux transports en commun, il faut développer des transports publics accessibles et de qualité : denses, fréquents, rapides, gratuits, définis par et pour les habitant.e.s. Contre les menaces actuelles de libéralisation, nous voulons défendre un bon statut social des salariés des transports en commun et la qualité du service rendu aux usagers. Contre la marchandisation de nos vies, par l’extension de la gratuité, à l’instar de l’éducation et de la santé, nous ferons ainsi grandir le sens de l’intérêt général et du service public.

Nous, signataires de cet appel, nous engageons à promouvoir la gratuité des  transports en commun dans toutes les villes et à peser sur les exécutifs à tous les niveaux, du local au national, afin qu’elle devienne l’un des leviers efficaces pour des avancées environnementales, sanitaires et sociales répondant aux défis du moment.

### LA GRATUITÉ DES TRANSPORTS PUBLICS LOCAUX EST UNE NÉCESSITÉ

<center>[Le lien vers l’appel](https://reseau-gratuite-transports.org)</center>