Title: Gouvernance Municipale
Date: 2017-01-01
Category: Nos Propositions
Slug: gouvernance-municipale
Photo: propositions/Gouvernance_municipale.png

- Diminution de 30% des indemnités pour le maire et ses adjoints
- Suppression des conseillers municipaux délégués 
- Référendum d’Initiative Communale (RIC) : à partir d’un seuil de 14 000 signatures
- Conseil de veille écologique incluant 50% de citoyens, 25% d’élus et 25% d’experts
- Audit des gestions municipales précédentes par une autorité indépendante
- Création et implantation de nouveaux panneaux d’expression libre et citoyenne
- Diminution du nombre des publications institutionnelles et création d’un journal annuel réservé à l’opposition
- Refonte des maisons de quartiers pour combiner activités socio-culturelles, travail associatif et aide aux démarches administratives
- Redéfinition du cadre et des missions des comités de quartiers