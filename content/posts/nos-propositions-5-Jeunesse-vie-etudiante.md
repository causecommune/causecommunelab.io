Title: Jeunesse & Vie étudiante
Date: 2014-01-01
Category: Nos Propositions
Photo: propositions/Jeunesse_vie_etudiante.png

- Accompagnement à la création de séjours pour les jeunes, de l’initiative à la réalisation
- Ouverture d’une auberge de jeunesse écologique, proche des axes de transports en commun
- Accès au vote dès 16 ans lors des consultations citoyennes
- Création de pôles multi-sports
- Mise en place d’hébergements d’urgence pour mineur.es et jeunes majeur.es en danger