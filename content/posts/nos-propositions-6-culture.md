Title: Culture
Date: 2012-01-01
Category: Nos Propositions
Slug: culture
Photo: propositions/Culture.png

- Résidences d'artistes en immersion dans tous les quartiers
- Création d'une friche urbaine sur le modèle de Darwin éco-système (Bordeaux)
- Remise à plat du système de subventions
- Réhabilitation du Petit Vélo pour en faire un lieu auto-géré par les compagnies
- Création en centre ville d'un nouveau lieu d'accueil pour les petites formes
- Création d'un festival de l'écologie et d'une journée "city bike" (jeu de piste culturel à vélo)
- Pérennisation des Rendez-vous Secrets 