Title: Urgence écologique
Date: 2019-01-01
Category: Nos Propositions
Slug: urgence-ecologique
Photo: propositions/Urgence_ecologique.png

## Une priorité absolue!

En matière de transition écologique, les déclarations d'intention ne servent à rien si elle ne sont pas suivies par de mesures concrètes. **Nous voulons agir, vite**. 

Dans la logique du "**penser global, agir local**", nous sommes persuadés que la ville de Clermont-Ferrand peut être un formidable laboratoire d'expériences en matière de lutte contre le réchauffement climatique. 

Nos propositions donnent des **réponses pragmatiques** qui permettent de créer une **ville résiliente** et **solidaire** en matière d'énergie, de transport, d'alimentation ou de cadre de vie. 


Faire de Clermont-Ferrand une ville écologiquement vertueuse n'est pas une mince affaire. Face à l'inertie des mandatures précédentes, l'enjeu est colossal. Rattrapons le temps perdu et faisons de l'**urgence climatique notre Cause Commune**. 

A l'instar de Nantes en 2013, Clermont-Ferrand peut et doit devenir une capitale verte européenne. 

De fait, les critères d'attribution de ce label correspondent au cahier des charges que nous nous sommes fixés pour les six ans à venir: atténuation et adaptation au changement climatique, mobilité urbaine durable, amélioration de la qualité de l'air et de l'eau, objectif zéro déchets, biodiversité, performance énergétique et éco-innovation.


- audit global visant à optimiser la sobriété énergétique pour le chauffage et l'isolation des bâtiments municipaux, l'éclairage public, la flotte de véhicules, les types d’équipement et les serres

- prise en compte du coefficient de biotope par surface dans tous les aménagements urbains et toutes les constructions

- végétalisation massive et augmentation des surfaces perméables

- multiplication des points d'eau en créant de nouvelles fontaines et bassins (miroirs d’eau)

- réduction drastique des déchets et systématisation du compostage en ville

- création d'une régie municipale de maraîchage urbain

- menus locavores et option végétarienne quotidienne dans les cantines 

- interdiction de tous les panneaux publicitaires lumineux 


