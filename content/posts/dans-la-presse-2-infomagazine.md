Title: Info Magazine
Date: 2019-11-27
Category: Dans la presse
Tags: municipales
Slug: info-magazine
Photo: presse/2-info-magazine.png
Summary: Cause Commune, la nouvelle démarche indépendante pour les municipales de Clermont-Ferrand
Ext_Url: https://www.infoclermont.fr/actualite-18183-cause-commune-entre-dans-le-jeu-politique-clermontois.html