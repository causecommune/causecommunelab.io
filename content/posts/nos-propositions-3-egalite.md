Title: &Eacute;galité
Date: 2016-01-01
Category: Nos Propositions
Slug: egalite
Photo: propositions/Egalite.png

- Création de foyers spécifiques aux femmes en situation de précarité ou de violence (hébergement, point hygiène, information, aide psychologique & juridique)
- Tables citoyenNES : état des lieux par et pour les femmes sur la redéfinition de l’espace public
- Travail avec les commerces, bars et cafés pour la prévention et l’action contre le harcèlement et les violences sexistes
- Mise en valeur du matrimoine (expositions, monuments, histoire de la ville, noms de rues)
- Formation régulière des agent.e.s de la mairie et de la police municipale sur les problématiques d'égalité et de lutte contre les violences sexistes et sexuelles
- Création de points de distribution de protections périodiques 