Title: Commerce
Date: 2011-01-01
Category: Nos Propositions
Slug: commerce
Photo: propositions/Commerce.png

- Animation institutionnelle des commerces de proximité avec une opération "un mois pour le commerce de proximité"
- Aménagement de l'espace urbain: généralisation des voies piétonnes en centre-ville et augmentation des terrasses et espaces verts
- Limitation de l'ouverture de nouvelles grandes enseignes et encadrement du travail du dimanche pour favoriser les petits commerces et acteurs locaux
- Mise en place d'un "conseil des commerçants et artisans clermontois" pour une cogestion de la politique municipale et préemption de locaux commerciaux pour redynamiser le tissu économique 
- Création d'une cellule d'aide et de conseil pour la rénovation énergétique des commerces