Title: Sauver ADP !
Date: 2019-09-15
Category: Actualités
Slug: sauver-adp
Photo: actualites/05-sauver-adp.png
Summary: Stop à la privatisation d’Aéroports de Paris

L’engagement citoyen et républicain qui anime la démarche Cause Commune devait nécessairement rencontrer le combat national contre la privatisation des Aéroports de Paris. 

Cause Commune a alors participé à la création du Collectif 63 — Stop à la privatisation d’Aéroports de Paris. 

Nous avons ainsi participé, avec un certain nombre d’autres organisations, à différents évènements pour recueillir des signatures pour le référendum ADP : diffusion de tracts, stands lors des manifestations Place de Jaude, happenings… 

<br />

## PHOTOS

<div id="diaporama">
<img src="/images/actualites/05-sauver-adp/1.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/2.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/3.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/4.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/5.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/6.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
<img src="/images/actualites/05-sauver-adp/7.jpg" alt="Stop à la privatisation d’Aéroports de Paris Sauver ADP" title="Stop à la privatisation d’Aéroports de Paris Sauver ADP">
</div>